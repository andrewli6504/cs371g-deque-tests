// -------------
// TestDeque.cpp
// -------------

// http://www.cplusplus.com/reference/deque/deque/

// --------
// includes
// --------

#include <deque>     // deque
#include <stdexcept> // invalid_argument, out_of_range

#include "gtest/gtest.h"

#include "Deque.hpp"

// ------
// usings
// ------

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct DequeFixture : Test {
    using deque_type    = T;
    using allocator_type = typename deque_type::allocator_type;
    using iterator       = typename deque_type::iterator;
    using const_iterator = typename deque_type::const_iterator;
};

using deque_types =
    Types<
            deque<int>,
            my_deque<int>,
            deque<int, allocator<int>>,
            my_deque<int, allocator<int>>
        >;

template <typename T>
struct NDequeFixture : Test {
    using deque_type    = T;
    using allocator_type = typename deque_type::allocator_type;
    using iterator       = typename deque_type::iterator;
    using const_iterator = typename deque_type::const_iterator;
};
using nested_deque_types =
    Types<
            deque<deque<int>>,
            my_deque<deque<int>>,
            deque<deque<int>, allocator<deque<int>>>,
            my_deque<deque<int>, allocator<deque<int>>>
    >;

#ifdef __APPLE__
    TYPED_TEST_CASE(DequeFixture, deque_types);
#else
    TYPED_TEST_CASE(DequeFixture, deque_types);
#endif

#ifdef __APPLE__
    TYPED_TEST_CASE(NDequeFixture, nested_deque_types);
#else
    TYPED_TEST_CASE(NDequeFixture, nested_deque_types);
#endif
// -----
// Tests
// -----

TYPED_TEST(DequeFixture, test0) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x;
    ASSERT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0u);}

TYPED_TEST(DequeFixture, test1) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    iterator b = begin(x);
    iterator e = end(x);
    EXPECT_TRUE(b == e);}

TYPED_TEST(DequeFixture, test2) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x;
    const_iterator b = begin(x);
    const_iterator e = end(x);
    EXPECT_TRUE(b == e);}

TYPED_TEST(DequeFixture, test3a) {
     using deque_type     = typename TestFixture::deque_type;
     using const_iterator = typename TestFixture::const_iterator;
     const deque_type x(3,6);
     const_iterator b = begin(x);
     const_iterator e = end(x);
     // *b = 3;
     ASSERT_TRUE(equal(b, e, begin({6, 6, 6})));
}

// ------------------
// deque(size_type s)
// ------------------

TYPED_TEST(DequeFixture, test3) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(10);
    ASSERT_EQ(x.size(), 10u);}


TYPED_TEST(DequeFixture, test4) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(0);
    ASSERT_EQ(x.size(), 0u);
    ASSERT_TRUE(x.empty());}

// -------------------------------------
// deque(size_type s, const_reference v)
// -------------------------------------

TYPED_TEST(DequeFixture, test5) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(3,5);
    ASSERT_EQ(x.size(), 3u);
    ASSERT_TRUE(equal(begin(x), end(x), begin({5, 5, 5})));}


TYPED_TEST(DequeFixture, test6) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(0, 5);  // edge case when size is 0
    ASSERT_EQ(x.size(), 0u);
    ASSERT_TRUE(x.empty());}

// --------------------------------------------------------------
// deque(size_type s, const_reference v, const allocator_type& a)
// --------------------------------------------------------------

TYPED_TEST(DequeFixture, test7) {
    using deque_type     = typename TestFixture::deque_type;
    using allocator_type = typename TestFixture::allocator_type;
    deque_type x(3,5, allocator_type());
    ASSERT_EQ(x.size(), 3u);
    ASSERT_TRUE(equal(begin(x), end(x), begin({5, 5, 5})));}


TYPED_TEST(DequeFixture, test8) {
    using deque_type     = typename TestFixture::deque_type;
    using allocator_type = typename TestFixture::allocator_type;
    deque_type x(0, 5, allocator_type());
    ASSERT_EQ(x.size(), 0u);
    ASSERT_TRUE(x.empty());}

// --------------------------------------------------------------
// deque(std::initializer_list<value_type> rhs)
// (std::initializer_list<value_type> rhs, const allocator_type& a)
// --------------------------------------------------------------

TYPED_TEST(DequeFixture, test9) {
    using deque_type     = typename TestFixture::deque_type;
    using allocator_type = typename TestFixture::allocator_type;
    const initializer_list<int> y = {1, 2, 3, 4, 5, 6, 1, 2, 3, 4, 5, 6};
    deque_type x(y, allocator_type());
    ASSERT_EQ(x.size(), 12);
    x.pop_back();
    ASSERT_TRUE(equal(begin(x), end(x), begin({1, 2, 3, 4, 5, 6, 1, 2, 3, 4, 5})));
    ASSERT_TRUE(equal(begin(y), end(y), begin({1, 2, 3, 4, 5, 6, 1, 2, 3, 4, 5, 6})));}


TYPED_TEST(DequeFixture, test10) {
    using deque_type     = typename TestFixture::deque_type;
    const initializer_list<int> y = {2, 3, 4};
    const deque_type x = y;
    ASSERT_NE(&x[0], &(y.begin()[0]));   // not the same address
    ASSERT_TRUE(equal(begin(x), end(x), begin({2,3,4})));}

// --------------------------------------------------------------
// deque(const my_deque& that)
// --------------------------------------------------------------


TYPED_TEST(DequeFixture, test11) {
    using deque_type     = typename TestFixture::deque_type;
    const deque_type y(10, 3);
    ASSERT_EQ(y.size(), 10);
    deque_type x(y);
    ASSERT_EQ(x.size(), 10);
    ASSERT_NE(&x[0], &y[0]);
    ASSERT_TRUE(equal(begin(x), end(x), begin(y)));
}


TYPED_TEST(DequeFixture, test12) {
    using deque_type     = typename TestFixture::deque_type;
    const deque_type y(0, 3);
    ASSERT_EQ(y.size(), 0);
    deque_type x(y);
    x.push_back(3);
    ASSERT_EQ(x.size(), 1);
    ASSERT_NE(&x[0], &y[0]);
    ASSERT_TRUE(equal(begin(x), end(x), begin({3})));
}

// --------------------------------------------------------------
// deque operator = (const my_deque& rhs)
// --------------------------------------------------------------

TYPED_TEST(DequeFixture, test13) {
    using deque_type     = typename TestFixture::deque_type;
    const deque_type y(999, 3);
    ASSERT_EQ(y.size(), 999);
    deque_type x = y;
    ASSERT_EQ(x.size(), 999);
    ASSERT_NE(&x[0], &y[0]);
    ASSERT_TRUE(equal(begin(x), end(x), begin(y)));}


TYPED_TEST(DequeFixture, test14) {
    using deque_type     = typename TestFixture::deque_type;
    const deque_type y(999, 3);
    const deque_type z(3, 999);
    deque_type x = y;
    ASSERT_TRUE(equal(begin(x), end(x), begin(y)));
    x = z;
    ASSERT_EQ(x.size(), 3);
    ASSERT_TRUE(equal(begin(x), end(x), begin(z)));
}

TYPED_TEST(DequeFixture, test51) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type y(999, 3);
    y = y;
    ASSERT_EQ(&y[0], &y[0]);
}

TYPED_TEST(DequeFixture, test52) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(999, 7);
    deque_type y(999, 3);
    x = y;
    ASSERT_TRUE(equal(begin(x), end(x), begin(y)));
}

TYPED_TEST(DequeFixture, test53) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(0, 7);
    deque_type y(5, 3);
    x = y;
    ASSERT_TRUE(equal(begin(x), end(x), begin(y)));
}

TYPED_TEST(DequeFixture, test55) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(5, 7);
    deque_type y(22, 3);
    x = y;
    ASSERT_TRUE(equal(begin(x), end(x), begin(y)));
    ASSERT_EQ(x.size(), 22);
}

// --------------------------------------------------------------
// deque operator [] (size_type index)
// --------------------------------------------------------------

TYPED_TEST(DequeFixture, test15) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(9, 10);
    x.push_back(-1);
    ASSERT_EQ(x[9], -1);}


TYPED_TEST(DequeFixture, test16) {
    using deque_type     = typename TestFixture::deque_type;
    const deque_type x(999, 1);
    ASSERT_EQ(x[998], 1);}

// --------------------------------------------------------------
// deque reference at (size_type index)
// throw out_of_range
// --------------------------------------------------------------

TYPED_TEST(DequeFixture, test17) {
    using deque_type     = typename TestFixture::deque_type;
    const deque_type x(999, 1);
    ASSERT_EQ(x.at(998), 1);}

TYPED_TEST(DequeFixture, test18) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(9, 10);
    ASSERT_EQ(x.size(), 9);
    try {
        ASSERT_EQ(x.at(9), 10);
        ASSERT_TRUE(false);}
    catch (const out_of_range&)
        {}
    ASSERT_EQ(x[8], x.at(8));}

// --------------------------------------------------------------
// deque back()
// --------------------------------------------------------------

TYPED_TEST(DequeFixture, test19) {
    using deque_type     = typename TestFixture::deque_type;
    const deque_type x(999, 1);
    ASSERT_EQ(x.back(), 1);}

TYPED_TEST(DequeFixture, test20) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(999, 1);
    x.push_back(8);
    ASSERT_EQ(x.back(), 8);
    x.pop_back();
    ASSERT_EQ(x.back(), 1);}

// --------------------------------------------------------------
// deque begin()
// --------------------------------------------------------------

TYPED_TEST(DequeFixture, test21) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(999, 1);
    ASSERT_EQ(*begin(x), 1);
    x.push_front(4);
    ASSERT_EQ(*begin(x), 4);}

TYPED_TEST(DequeFixture, test22) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const initializer_list<int> y = {2, 3, 4};
    const deque_type x(y);
    const_iterator b = begin(x);
    ASSERT_EQ(*b, 2);
    ASSERT_EQ(*(++b), 3);}

// --------------------------------------------------------------
// deque clear()
// --------------------------------------------------------------

TYPED_TEST(DequeFixture, test23) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(3, 5);
    ASSERT_TRUE(equal(begin(x), end(x), begin({5,5,5})));
    x.clear();
    ASSERT_TRUE(x.empty());}

TYPED_TEST(DequeFixture, test24) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(3, 5);
    ASSERT_TRUE(equal(begin(x), end(x), begin({5,5,5})));
    x.clear();
    ASSERT_TRUE(x.empty());
    const initializer_list<int> y = {2, 3, 4};
    x = y;
    ASSERT_EQ(*begin(x), 2);
    ASSERT_TRUE(equal(begin(x), end(x), begin(y)));}

// --------------------------------------------------------------
// deque empty()
// --------------------------------------------------------------

TYPED_TEST(DequeFixture, test25) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(0);
    ASSERT_TRUE(x.empty());}

TYPED_TEST(DequeFixture, test26) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(3, 5);
    ASSERT_TRUE(!x.empty());}

// --------------------------------------------------------------
// deque end()
// --------------------------------------------------------------

TYPED_TEST(DequeFixture, test27) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(999, 1);
    ASSERT_EQ(*(--end(x)), 1);  //end() is one over
    x.push_back(4);
    ASSERT_EQ(*(--end(x)), 4);}

TYPED_TEST(DequeFixture, test28) {
    using deque_type     = typename TestFixture::deque_type;
    const initializer_list<int> y = {2, 3, 4};
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x(y);
    const_iterator e = end(x);
    ASSERT_EQ(*(----e), 3);
    ASSERT_EQ(*(--e), 2);}

// --------------------------------------------------------------
// deque erase()
// --------------------------------------------------------------

TYPED_TEST(DequeFixture, test29) {
    using deque_type     = typename TestFixture::deque_type;
    initializer_list<int> y = {2, 3, 4};
    deque_type x(y);
    x.erase(begin(x));
    ASSERT_EQ(*(begin(x)), 3);
    ASSERT_EQ(x.size(), 2);}

TYPED_TEST(DequeFixture, test30) {
    using deque_type     = typename TestFixture::deque_type;
    initializer_list<int> y = {2, 3, 4};
    deque_type x(y);
    x.erase(----end(x));     // earse the middle
    ASSERT_EQ(*(--end(x)), 4);
    ASSERT_EQ(x.size(), 2);
    ASSERT_TRUE(equal(begin(x), end(x), begin({2,4})));}

// --------------------------------------------------------------
// deque front()
// --------------------------------------------------------------

TYPED_TEST(DequeFixture, test31) {
    using deque_type     = typename TestFixture::deque_type;
    const deque_type x(999, 1);
    ASSERT_EQ(x.front(), 1);}

TYPED_TEST(DequeFixture, test32) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(999, 1);
    x.push_front(8);
    ASSERT_EQ(x.front(), 8);
    x.pop_front();
    ASSERT_EQ(x.front(), 1);}

// --------------------------------------------------------------
// deque insert()
// --------------------------------------------------------------

TYPED_TEST(DequeFixture, test33) {
    using deque_type     = typename TestFixture::deque_type;
    initializer_list<int> y = {2, 3, 4};
    deque_type x(y);
    x.insert(++begin(x), 9);
    ASSERT_TRUE(equal(begin(x), end(x), begin({2,9,3,4})));}

TYPED_TEST(DequeFixture, test34) {
    using deque_type     = typename TestFixture::deque_type;
    initializer_list<int> y = {2, 3, 4};
    deque_type x(y);
    x.insert(end(x), 9);
    ASSERT_TRUE(equal(begin(x), end(x), begin({2,3,4,9})));}

TYPED_TEST(DequeFixture, test34a) {
    using deque_type     = typename TestFixture::deque_type;
    initializer_list<int> y = {2, 3, 4};
    deque_type x(y);
    x.insert(begin(x), 9);
    ASSERT_TRUE(equal(begin(x), end(x), begin({9,2,3,4})));}
// --------------------------------------------------------------
// deque pop_back()
// --------------------------------------------------------------

TYPED_TEST(DequeFixture, test35) {
    using deque_type     = typename TestFixture::deque_type;
    initializer_list<int> y = {2, 3, 4};
    deque_type x(y);
    x.pop_back();
    ASSERT_TRUE(equal(begin(x), end(x), begin({2,3})));}

TYPED_TEST(DequeFixture, test36) {
    using deque_type     = typename TestFixture::deque_type;
    initializer_list<int> y = {2, 3, 4};
    deque_type x(y);
    x.pop_back();
    x.pop_back();
    x.pop_back();
    ASSERT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0);}

// --------------------------------------------------------------
// deque pop_front()
// --------------------------------------------------------------

TYPED_TEST(DequeFixture, test37) {
    using deque_type     = typename TestFixture::deque_type;
    initializer_list<int> y = {2, 3, 4};
    deque_type x(y);
    x.pop_front();
    ASSERT_TRUE(equal(begin(x), end(x), begin({3,4})));}

TYPED_TEST(DequeFixture, test38) {
    using deque_type     = typename TestFixture::deque_type;
    initializer_list<int> y = {2, 3, 4};
    deque_type x(y);
    x.pop_front();
    x.pop_front();
    x.pop_front();
    ASSERT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0);}

// --------------------------------------------------------------
// deque push_back()
// --------------------------------------------------------------

TYPED_TEST(DequeFixture, test39) {
    using deque_type     = typename TestFixture::deque_type;
    initializer_list<int> y = {2, 3, 4};
    deque_type x(y);
    x.push_back(5);
    ASSERT_TRUE(equal(begin(x), end(x), begin({2,3,4,5})));}

TYPED_TEST(DequeFixture, test40) {
    using deque_type     = typename TestFixture::deque_type;
    initializer_list<int> y = {2, 3, 4};
    deque_type x(y);
    x.push_back(7);
    x.push_back(7);
    x.push_back(7);
    ASSERT_TRUE(!x.empty());
    ASSERT_EQ(x.size(), 6);}

// --------------------------------------------------------------
// deque push_front()
// --------------------------------------------------------------

TYPED_TEST(DequeFixture, test41) {
    using deque_type     = typename TestFixture::deque_type;
    initializer_list<int> y = {2, 3, 4};
    deque_type x(y);
    x.push_front(5);
    ASSERT_TRUE(equal(begin(x), end(x), begin({5,2,3,4})));}

TYPED_TEST(DequeFixture, test42) {
    using deque_type     = typename TestFixture::deque_type;
    initializer_list<int> y = {2, 3, 4};
    deque_type x(y);
    x.push_front(7);
    x.push_front(7);
    x.push_front(7);
    ASSERT_TRUE(!x.empty());
    ASSERT_EQ(x.size(), 6);}


// --------------------------------------------------------------
// deque resize()
// --------------------------------------------------------------

TYPED_TEST(DequeFixture, test43) {
    using deque_type     = typename TestFixture::deque_type;
    initializer_list<int> y = {2, 3, 4};
    deque_type x(y);
    x.resize(5, 4);       // bigger size, with 4
    x.push_back(9);
    ASSERT_TRUE(equal(begin(x), end(x), begin({2,3,4,4,4,9})));}

TYPED_TEST(DequeFixture, test44) {
    using deque_type     = typename TestFixture::deque_type;
    initializer_list<int> y = {2, 3, 4};
    deque_type x = y;
    x.resize(3);   // same size
    ASSERT_TRUE(equal(begin(x), end(x), begin(y)));}

TYPED_TEST(DequeFixture, test45) {
    using deque_type     = typename TestFixture::deque_type;
    initializer_list<int> y = {2, 3, 4, 5, 6};
    deque_type x = y;
    x.resize(3, 4);   // smaller
    ASSERT_TRUE(equal(begin(x), end(x), begin({2,3,4})));}

TYPED_TEST(DequeFixture, test46) {
    using deque_type     = typename TestFixture::deque_type;
    initializer_list<int> y = {2, 3, 4};
    deque_type x(y);
    x.resize(5);       // bigger size, with 0
    x.push_back(9);
    ASSERT_TRUE(equal(begin(x), end(x), begin({2,3,4,0,0,9})));}

TYPED_TEST(DequeFixture, test47) {
    using deque_type     = typename TestFixture::deque_type;
    initializer_list<int> y = {2, 3, 4};
    deque_type x(y);
    x.resize(0);       // resize 0
    ASSERT_TRUE(x.empty());}


// --------------------------------------------------------------
// deque size()
// --------------------------------------------------------------

TYPED_TEST(DequeFixture, test48) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(0,1);
    ASSERT_TRUE(x.size() == 0);}

TYPED_TEST(DequeFixture, test49) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(999,1);
    ASSERT_TRUE(x.size() == 999);
    x.resize(5);
    ASSERT_TRUE(x.size() == 5);}

// --------------------------------------------------------------
// deque swap()
// --------------------------------------------------------------

TYPED_TEST(DequeFixture, test50) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(0,1);
    deque_type y(999,1);
    x.swap(y);
    ASSERT_TRUE(x.size() == 999);
    ASSERT_TRUE(y.size() == 0);}

// --------------------------------------------------------------
// deque of deque
// --------------------------------------------------------------

TYPED_TEST(NDequeFixture, test100) {
    using deque_type = typename TestFixture::deque_type;
    deque<int> x(2, 1);
    deque_type y(3, x);
    ASSERT_TRUE(y.size() == 3);
}

TYPED_TEST(NDequeFixture, test101) {
    using deque_type = typename TestFixture::deque_type;
    deque<int> x(2, 1);
    deque_type y(3, x);
    y.push_back(x);
    ASSERT_TRUE(y.size() == 4);
    y.pop_back();
    ASSERT_TRUE(y.size() == 3);
    y.pop_front();
    ASSERT_TRUE(y.size() == 2);
}


TYPED_TEST(NDequeFixture, test102) {
    using deque_type = typename TestFixture::deque_type;
    deque<int> x(2, 1);
    deque<int> z(9, 4);
    deque_type y(3, x);
    y.push_front(z);
    ASSERT_TRUE(y[0].size() == 9);
    deque<int> a = y.at(0);
    ASSERT_TRUE(equal(a.begin(), a.end(), begin({4, 4, 4, 4, 4, 4, 4, 4, 4})));
    ASSERT_NE(&y[0][0], &z[0]);
    z.pop_back();
    ASSERT_TRUE(equal(a.begin(), a.end(), begin({4, 4, 4, 4, 4, 4, 4, 4, 4})));

}

// insert and erase
TYPED_TEST(NDequeFixture, test103) {
    using deque_type = typename TestFixture::deque_type;
    deque<int> x(2, 1);
    deque<int> z(4, 13);
    deque_type y(6, x);
    y.erase(--y.end());
    ASSERT_TRUE(y.size() == 5);
    using iterator   = typename TestFixture::iterator;
    iterator it = y.begin();
    it++;
    it++;
    it++;
    y.insert(it, z);
    deque<int> a = y.at(3);
    ASSERT_TRUE(equal(a.begin(), a.end(), begin({13, 13, 13, 13})));

    y.erase(it);
    deque<int> b = y.at(3);
    ASSERT_TRUE(equal(b.begin(), b.end(), begin({1, 1})));
}

// swap and clear
TYPED_TEST(NDequeFixture, test104) {
    using deque_type = typename TestFixture::deque_type;
    deque<int> x(2, 1);
    deque<int> z(4, 13);
    deque_type y(5, x);
    deque_type a(8, z);

    y.swap(a);
    ASSERT_TRUE(y.size() == 8);
    ASSERT_TRUE(a.size() == 5);

    y.clear();
    ASSERT_TRUE(y.size() == 0);
    ASSERT_TRUE(a.size() == 5);
}


// resize
TYPED_TEST(NDequeFixture, test105) {
    using deque_type = typename TestFixture::deque_type;
    deque<int> x(2, 1);
    deque<int> z(4, 13);
    deque_type y(5, x);
    deque_type a(8, z);

    y.resize(100, z);
    ASSERT_TRUE(y.size() == 100);
}

TYPED_TEST(DequeFixture, test106) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(5, 6);
    x.resize(22, 3);
    }
