// -------------
// TestDeque.cpp
// -------------

// http://www.cplusplus.com/reference/deque/deque/

// --------
// includes
// --------

#include <deque>     // deque
#include <stdexcept> // invalid_argument, out_of_range

#include "gtest/gtest.h"

#include "Deque.hpp"

// ------
// usings
// ------

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct DequeFixture : Test {
    using deque_type    = T;
    using allocator_type = typename deque_type::allocator_type;
    using iterator       = typename deque_type::iterator;
    using const_iterator = typename deque_type::const_iterator;
};

using deque_types =
    Types<
    deque<int>,
    my_deque<int>,
    deque<int, allocator<int>>,
    my_deque<int, allocator<int>>>;

using deque_deque_types =
    Types<
    deque<deque<int>>,
    my_deque<deque<int>>,
    deque<deque<int>, allocator<deque<int>>>,
    my_deque<deque<int>, allocator<deque<int>>>>;


#ifdef __APPLE__
TYPED_TEST_CASE(DequeFixture, deque_types,);
#else
TYPED_TEST_CASE(DequeFixture, deque_types);
#endif

// -----
// Tests
// -----

// size constructor
TYPED_TEST(DequeFixture, test0) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(100);
    ASSERT_EQ(x.size(), 100u);
}

// size, initial value constructor
TYPED_TEST(DequeFixture, test1) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(100, 5);
    ASSERT_EQ(x.size(), 100u);
    ASSERT_EQ(x[50], 5);
}

// initalizer list constructor
TYPED_TEST(DequeFixture, test3) {
    using deque_type = typename TestFixture::deque_type;
    initializer_list<int> y = {1, 2, 3, 4, 5};
    deque_type x(y);
    ASSERT_EQ(x.size(), y.size());
    ASSERT_EQ(x[3], 4);
}

// assignment operator
TYPED_TEST(DequeFixture, test4) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(100, 5);
    deque_type y(50, 3);
    x = y;
    ASSERT_EQ(x.size(), y.size());
    ASSERT_EQ(x[1],y[1]);
    ASSERT_EQ(x[10],y[10]);
}

// copy constructor
TYPED_TEST(DequeFixture, test5) {
    using deque_type = typename TestFixture::deque_type;
    initializer_list<int> y = {1, 2, 3, 4, 5};
    deque_type x(y);
    deque_type z(x);
    ASSERT_EQ(x[2], z[2]);
    ASSERT_EQ(x[3], z[3]);
}

// push back
TYPED_TEST(DequeFixture, test6) {
    using deque_type = typename TestFixture::deque_type;
    initializer_list<int> y = {1, 2, 3};
    deque_type x(y);
    x.push_back(4);
    x.push_back(5);
    x.push_back(6);
    x.push_back(7);
    x.push_back(8);
    x.push_back(9);
    ASSERT_EQ(x[3], 4);
    ASSERT_EQ(x[4], 5);
    ASSERT_EQ(x[5], 6);
    ASSERT_EQ(x[6], 7);
    ASSERT_EQ(x[7], 8);
    ASSERT_EQ(x[8], 9);
    ASSERT_EQ(x.size(), 9);
}

// push back many times
TYPED_TEST(DequeFixture, test7) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    for(int i = 0; i < 100; ++i) {
        x.push_back(i);
    }
    for(int i = 0; i < 100; ++i) {
        ASSERT_EQ(x[i], i);
    }
    ASSERT_EQ(x.size(), 100);
}

// back
TYPED_TEST(DequeFixture, test8) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_back(1);
    ASSERT_EQ(x.back(), 1);
    x.push_back(2);
    ASSERT_EQ(x.back(), 2);
    x.push_back(3);
    ASSERT_EQ(x.back(), 3);
}

// pop back, back
TYPED_TEST(DequeFixture, test9) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1,2,3,4,5});
    ASSERT_EQ(x.back(), 5);
    x.pop_back();
    ASSERT_EQ(x.back(), 4);
    x.pop_back();
    ASSERT_EQ(x.back(), 3);
    x.pop_back();
    ASSERT_EQ(x.size(), 2);
}

//push front, front
TYPED_TEST(DequeFixture, test10) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_front(5);
    ASSERT_EQ(x.front(), 5);
    x.push_front(4);
    ASSERT_EQ(x.front(), 4);
    x.push_front(3);
    ASSERT_EQ(x.front(), 3);
    x.push_front(2);
    ASSERT_EQ(x.front(), 2);
    x.push_front(1);
    ASSERT_EQ(x.front(), 1);
    ASSERT_EQ(x.size(), 5);
}

//push front many times
TYPED_TEST(DequeFixture, test11) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    for(int i = 100; i > 0; --i) {
        x.push_front(i);
    }
    for(int i = 0; i < 100; ++i) {
        ASSERT_EQ(x[i], i + 1);
    }
    ASSERT_EQ(x.size(), 100);
}

//pop front, front
TYPED_TEST(DequeFixture, test12) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1,2,3,4,5});
    x.pop_front();
    ASSERT_EQ(x.front(), 2);
    x.pop_front();
    ASSERT_EQ(x.front(), 3);
    x.pop_front();
    ASSERT_EQ(x.front(), 4);
    x.pop_front();
    ASSERT_EQ(x.front(), 5);
}

// insert
TYPED_TEST(DequeFixture, test13) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1,2,3,4,5});

    x.insert(x.begin() + 2, 9); // 1,2,9,3,4,5
    x.insert(x.begin() + 3, 13);// 1,2,9,13,3,4,5
    ASSERT_EQ(x[2], 9);
    ASSERT_EQ(x[3], 13);

    x.insert(x.begin(), 5);
    x.insert(x.end(), 6);
    ASSERT_EQ(x[0], 5);
    ASSERT_EQ(x[8], 6);
}

// == operator
TYPED_TEST(DequeFixture, test14) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1,2,3,4,5});
    deque_type y({1,2,3,4,5});
    ASSERT_TRUE(x == y);
}

// == operator
TYPED_TEST(DequeFixture, test15) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1,2,3,4,5});
    deque_type y({1,2,3,4,6});
    ASSERT_TRUE(x != y);
}

// == operator
TYPED_TEST(DequeFixture, test16) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1,2,3,4,5});
    deque_type y({1,2});
    ASSERT_FALSE(x == y);
    y.push_back(3);
    y.push_back(4);
    y.push_back(5);
    ASSERT_TRUE(x == y);
}

// < operator
TYPED_TEST(DequeFixture, test17) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1,2,3,4,5});
    deque_type y({1,2,3,4,6});
    ASSERT_TRUE(x < y);
}

// < operator
TYPED_TEST(DequeFixture, test18) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1,2,3,4,7});
    deque_type y({1,2,3,4,6});
    ASSERT_FALSE(x < y);
}

// clear
TYPED_TEST(DequeFixture, test19) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1,2,3,4,7});
    x.clear();
    ASSERT_EQ(x.size(), 0);
    ASSERT_TRUE(x.empty());
}

// erase
TYPED_TEST(DequeFixture, test20) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1,2,3,4,5});
    x.erase(x.begin() + 2);
    ASSERT_EQ(x.size(), 4);
    ASSERT_EQ(x.at(2), 4);
    x.erase(x.begin() + 2);
    ASSERT_EQ(x.size(), 3);
    ASSERT_EQ(x.at(2), 5);
}

//make sure growing only copies outer pointers, not inner values (push back)
TYPED_TEST(DequeFixture, test21) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    int* y[200];
    for(int i = 0; i < 200; ++i) {
        x.push_back(i);
        y[i] = &x[i];
    }
    for(int i = 0; i < 200; ++i) {
        ASSERT_EQ(y[i], &x[i]);
    }
}

//make sure growing only copies outer pointers, not inner values (push front)
TYPED_TEST(DequeFixture, test22) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    int* y[200];
    for(int i = 200; i >= 0; --i) {
        x.push_front(i);
        y[i] = &x[0];
    }
    for(int i = 0; i < 200; ++i) {
        ASSERT_EQ(y[i], &x[i]);
    }
}

// at, out of bounds
TYPED_TEST(DequeFixture, test23) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1,2,3,4,5});
    try {
        int y = x.at(5);
        ++y;
        ASSERT_TRUE(false);
    }
    catch(const exception &e) {
        ASSERT_TRUE(true);
    }
}

// at, out of bounds
TYPED_TEST(DequeFixture, test24) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x({1,2,3,4,5});
    try {
        int y = x.at(-1);
        ++y;
        ASSERT_TRUE(false);
    }
    catch(const exception &e) {
        ASSERT_TRUE(true);
    }
}

// erase, begin, end
TYPED_TEST(DequeFixture, test25) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1,2,3,4,5});
    deque_type y({1,2,4,5});
    x.erase(x.begin() + 2);
    ASSERT_TRUE(x == y);
    ASSERT_EQ(x.size(),4);
    x.erase(x.begin());
    ASSERT_EQ(x[0], 2);
    x.erase(x.end());
    ASSERT_EQ(x[1], 4);
}

// erase
TYPED_TEST(DequeFixture, test26) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(100, 1);
    deque_type y;
    for(int i = 0; i < 100; i++) {
        x.erase(x.begin());
    }
    ASSERT_TRUE(x == y);
    deque_type z(100, 1);
    for(int i = 0; i < 98; i++) {
        z.erase(z.begin() + 1);
    }
    ASSERT_EQ(z.size(), 2);
}

// begin, end
TYPED_TEST(DequeFixture, test27) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.insert(x.begin(), 3);
    x.insert(x.end(), 3);
    ASSERT_EQ(x.size(), 2);
    ASSERT_EQ(x[0], 3);
    ASSERT_EQ(x[1], 3);
}

// begin, end, default constructor
TYPED_TEST(DequeFixture, test28) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    ASSERT_TRUE(x.begin() == x.end());
}

// front, back
TYPED_TEST(DequeFixture, test29) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x{1};
    ASSERT_TRUE(x.front() == x.back());
}

// front, back
TYPED_TEST(DequeFixture, test30) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1,2,3});
    ASSERT_EQ(x.front(), 1);
    ASSERT_EQ(x.back(), 3);
}

// swap
TYPED_TEST(DequeFixture, test31) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x({1,2,3});
    deque_type y({3,2,1});
    deque_type z({1,2,3});
    ASSERT_EQ(x,z);
    ASSERT_FALSE(x == y);
    y.swap(z);
    ASSERT_EQ(x,y);
    ASSERT_FALSE(x == z);
}

// const iterators
TYPED_TEST(DequeFixture, test32) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x{1};
    const_iterator b = begin(x);
    const_iterator e = end(x);
    EXPECT_TRUE(b == --e);
}

// const iterators
TYPED_TEST(DequeFixture, test33) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x{1};
    const_iterator b = begin(x);
    const_iterator e = end(x);
    EXPECT_TRUE(++b == e);
}

// const iterators
TYPED_TEST(DequeFixture, test34) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x{1,2,3};
    const_iterator b = begin(x);
    const_iterator e = end(x);
    EXPECT_TRUE(b + 3 == e);
}

// const iterators
TYPED_TEST(DequeFixture, test35) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x{1,2,3};
    const_iterator b = begin(x);
    const_iterator e = end(x);
    EXPECT_TRUE(b == e - 3);
}

// resize
TYPED_TEST(DequeFixture, test36) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x{1,2,3};
    x.resize(100);
    ASSERT_EQ(x[0], 1);
    ASSERT_EQ(x[50], 0);
}

// resize
TYPED_TEST(DequeFixture, test37) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x;
    x.resize(100, 5);
    deque_type y(100,5);
    ASSERT_TRUE(x == y);
}

// pushing and popping different sides
TYPED_TEST(DequeFixture, test38) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x;
    deque_type y;
    for(int i = 0 ; i < 100; ++i) {
        x.push_front(1);
    }
    for(int i = 0 ; i < 100; ++i) {
        x.pop_front();
    }
    ASSERT_TRUE(x == y);

    for(int i = 0 ; i < 100; ++i) {
        x.push_front(1);
    }
    for(int i = 0 ; i < 100; ++i) {
        x.pop_back();
    }
    ASSERT_TRUE(x == y);
}

// pushing and popping different sides
TYPED_TEST(DequeFixture, test39) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x;
    deque_type y;
    for(int i = 0 ; i < 100; ++i) {
        x.push_back(1);
    }
    for(int i = 0 ; i < 100; ++i) {
        x.pop_back();
    }
    ASSERT_TRUE(x == y);

    for(int i = 0 ; i < 100; ++i) {
        x.push_back(1);
    }
    for(int i = 0 ; i < 100; ++i) {
        x.pop_front();
    }
    ASSERT_TRUE(x == y);
}

// at
TYPED_TEST(DequeFixture, test40) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x({1,2,3,4,5});
    for(int i = 0; i < 5; ++i) {
        ASSERT_EQ(x.at(i), i+1 );
        ASSERT_EQ(x.at(i), x[i]);
    }
}

// resize
TYPED_TEST(DequeFixture, test41) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x;
    for(int i = 1; i < 100; ++i) {
        x.push_back(i);
    }
    x.resize(5);
    deque_type y({1,2,3,4,5});
    ASSERT_TRUE(x == y);
}

// resize, clear
TYPED_TEST(DequeFixture, test42) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x({1,2,3,4,5});
    deque_type y({1,2,3,4,5});
    x.resize(0);
    y.clear();
    ASSERT_TRUE(x == y);
}

// inserting, pushing, and popping many
TYPED_TEST(DequeFixture, test43) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x;
    deque_type y;
    for(int i = 0; i < 100; ++i) {
        x.push_back(1);
        y.push_front(1);
    }
    for(int i = 0; i < 100; ++i) {
        x.insert(x.begin() + 50, 2);
        y.insert(y.begin() + 50, 2);
    }
    ASSERT_TRUE(x == y);
}

// swapping multiple times
TYPED_TEST(DequeFixture, test44) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x({1,2,3});
    deque_type y({4,5,6});
    deque_type z({1,2,3});
    ASSERT_TRUE(x == z);
    y.swap(z);
    x.push_back(4);
    y.push_back(4);
    ASSERT_TRUE(x == y);
    y.swap(z);
    ASSERT_TRUE(x == z);
}